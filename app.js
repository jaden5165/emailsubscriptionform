var myapp = angular.module('myapp', ['firebase', 'ngMessages', 'angularSpinners', 'vcRecaptcha']);
//myapp.config(function ($httpProvider) {
//  $httpProvider.defaults.headers.common = {};
//  $httpProvider.defaults.headers.post = {};
//  $httpProvider.defaults.headers.put = {};
//  $httpProvider.defaults.headers.patch = {};
//});
myapp.controller('formCtrl', function ($scope, $firebaseArray, $http, spinnerService, vcRecaptchaService) {
    console.log("in formCtrl");

    $scope.recaptchaKey = '6LcUdRsTAAAAAE8mM_RQpbhZABYUk_SOQE3dbqbR';

    var ref = new Firebase("https://emailsubscribeform.firebaseio.com");
    var subRef = ref.child("subscribers");
    var subscriber = $firebaseArray(subRef);

    //    $scope.setResponse = function (response) {
    //        // send the `response` to your server for verification.
    //        if(response !== "")
    //            $scope.subButton = false;
    //    };

    $scope.addSubscriber = function () {
        if ($scope.email) {
            if (vcRecaptchaService.getResponse() === "") { //if string is empty
                alert("Please tick the robot then subscribe!");
            } else {
                spinnerService.show('html5spinner');

                var post_data = { //prepare payload for request
                    'g-recaptcha-response': vcRecaptchaService.getResponse() //send g-captcah-reponse to our server
                };

                //$http.post("http://localhost:3000/recaptcha/verify", post_data)
                $http.post("http://heroku-nodejs-backend.herokuapp.com/recaptcha/verify", post_data)
                    .success(function (data, status) {

                    //console.log("success");
                    //console.log("status:" + status);
                    console.log("data:", data);

                    var data = {
                        contactEmail: $scope.email
                    };

                    //$http.post("http://localhost:3000/emails/confirmSubscribe", data)
                    $http.post("http://heroku-nodejs-backend.herokuapp.com/emails/confirmSubscribe", data)
                        .success(function (data, status) {
                        //console.log("success");
                        //console.log(data);
                        //console.log(status);
                        if (data === "true") {

                            //Add to Firebase
                            subscriber.$add({
                                email: $scope.email
                            }).then(function (subref) {
                                var id = subref.key();
                                console.log("added record with id " + id + " email:" + $scope.email);

                                spinnerService.hide('html5spinner');
                                $scope.subscribed = true;
                                $scope.email = "";
                                //reset recaptcha
                                vcRecaptchaService.reload();
                            });
                        } else {
                            alert("SERVER ERROR: Email cannot be sent.")
                            spinnerService.hide('html5spinner');
                            $scope.email = "";
                            //reset recaptcha
                            vcRecaptchaService.reload();
                        }

                    })
                        .error(function (data, status) {
                        console.log("error");
                        console.log(data);
                        console.log(status);
                    });
                })
                    .error(function (data, status) {
                    console.log("error");
                    console.log(data);
                    console.log(status);
                });
            }
        }
    };

});


//myapp.factory('fbService', function($scope){
//    $scope.test = 0;
//});