# Email Subscription Form #

### This is a email subscription template written in AngularJS framework. ###
* Integrated FIREBASE - AngularFire
* Invoke NodeJS API hosted at HEROKU to send confirmation email.

### Firebase ###
* Firebase will be used to store subscriber's email


### /sendEmail API will be invoked to send email ###
* /sendEmail API hosted at HEROKU
* Written with NodeJS + ExpressJS + NodeMailer
* Backend sendEmail source code is located @ https://bitbucket.org/jaden5165/nodemailer_dev

### angularSpinners ###
*Make use of angularSpinners to prompt loading when click subscribe
*More details @ http://codetunnel.io/how-to-do-loading-spinners-the-angular-way/